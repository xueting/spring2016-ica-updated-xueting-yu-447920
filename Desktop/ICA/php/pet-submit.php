<?php
/**
 * Created by PhpStorm.
 * User: xueting
 * Date: 3/2/16
 * Time: 12:28 PM
 */


    function test_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    if(!isset($_POST['species']) || !isset($_POST['name']) || !isset($_POST['weight'])
        ||!isset($_POST['description']) ){
        die("ERROR MISSING POST VARIABLES");
    }

    $species = (string) test_input($_POST['species']);
    $name = (string) test_input($_POST['name']);
    $weight = (float) test_input($_POST['weight']);
    $description = (string) test_input($_POST['description']);


    $target_dir = "home/xueting/Desktop/pics/";
    $target_file = $target_dir . basename($_FILES["picture"]["tmp_name"]);
    $uploadOk = 1;
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
    // Check if image file is a actual image or fake image
    if(isset($_POST["submit"])) {
        $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
        if($check !== false) {
            echo "File is an image - " . $check["mime"] . ".";
            $uploadOk = 1;
        } else {
            echo "File is not an image.";
            $uploadOk = 0;
        }
    }

    $filepath = $target_file;
    require('connectDB.php');

    $stmt = $mysqli->prepare("insert into pets (species, name, weight, description, filename) values (?, ?, ?, ?, ?)");

    if (!$stmt) {
        printf("Query failed: %s\n", $mysqli->error);
        exit;
    }
    $stmt->bind_param('ssdss', $species, $name, $weight, $description, $filepath);
    $stmt->execute();
    $stmt->close();


    if (move_uploaded_file($_FILES['picture']['name'], $filepath) ){
        header("Location: add-pet.html");
        exit;
    } else {
        echo "failed";
        header("Location: add-pet.html");
        exit;
    }

?>