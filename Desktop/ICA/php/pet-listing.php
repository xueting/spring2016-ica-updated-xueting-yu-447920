<?php

    function test_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title>Pet Listings</title>
    <style type="text/css">
        body{
            width: 760px; /* how wide to make your web page */
            background-color: teal; /* what color to make the background */
            margin: 0 auto;
            padding: 0;
            font:12px/16px Verdana, sans-serif; /* default font */
        }
        div#main{
            background-color: #FFF;
            margin: 0;
            padding: 10px;
        }
    </style>
</head>

<div class="blog-main">

    <div class="col-sm-8 blog-main">
        <h1>Pet Listings</h1>

        <?php



        require('connectDB.php');
        $value = array("cat", "dog", "fish", "bird", "hamster");
        foreach ($value as $type) {

            $stmt = $mysqli->prepare("select species, name, weight, description, filename from pets where species =?");
            if (!$stmt) {
                printf("Query failed: %s\n", $mysqli->error);
                exit;
            }
            $type = test_input($type);
            $stmt->bind_param('s', $type);
            $stmt->execute();
            $stmt->bind_result($species, $name, $weight, $description, $filepath);
            $result = $stmt->get_result();
            while ($row = $result->fetch_assoc()) {
                echo "<li>".htmlspecialchars($row["species"]. " :  \"". $row["name"]. " is " . $row["weight"])."</li>\"<br\>";
                echo "<li>Description: ".htmlspecialchars($row["description"])."</li>\"<br\>";
            }
            $stmt->close();
            $stmt = $mysqli->prepare("select count(*), avg(weight) from pets where species = ?");
            echo $type;
            $stmt->bind_param('s', $type);
            $stmt->execute();
            $stmt->bind_result($count, $aweight);
            $stmt->fetch();
            echo "<li> The total amount of pets are ".htmlspecialchars($count)." and the average weight is :". htmlspecialchars($aweight)."</li>";

            $stmt->close();

        }



        ?>

        <a href = add-pet.html>Submit a new pet info</a>

    </div>
</div>


/**
 * Created by PhpStorm.
 * User: xueting
 * Date: 3/2/16
 * Time: 12:49 PM
 */